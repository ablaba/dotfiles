" Required for vimwiki 
set nocompatible
filetype plugin on
syntax on
 
let g:vimwiki_list = [{'path': '~/vimwiki/',
             \ 'syntax': 'markdown', 'ext': '.md'}]
 
" General preferences
set tabstop=4
set expandtab
set number

" Remap escape to jk
inoremap jk <ESC>

" Change leader key from \ to '
let mapleader ="'"

set noswapfile
set hlsearch
set ignorecase
set incsearch
